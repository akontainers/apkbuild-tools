#!/usr/bin/env sh

set -eu

createTraps() {
  trap "echo Container stopped" EXIT QUIT INT TERM
}

getStopDate() {
  timerName="$1"
  waitTime="$2"
  fileDate="$(date -r "$timerName" +%s)"
  echo "$((fileDate + waitTime))"
}

setTimer() {
  timerName="$1"
  waitTime="$2"
  touch "$timerName"

  while [ "$(date +%s)" -lt "$(getStopDate "$timerName" "$waitTime")" ]; do
    sleep 10 &
    wait $!
  done
}

updateTimer() {
  timerName="$1"
  touch "$timerName"
}

main() {
  command="$1"
  timerName="/tmp/timer.$(hostname)"

  case "$command" in
  "wait")
    waitTime=120
    if [ "$#" -eq 2 ]; then
      waitTime="$2"
    fi
    createTraps
    setTimer "$timerName" "$waitTime"
    ;;
  "update")
    updateTimer "$timerName"
    ;;
  *) echo "Unknown command" ;;
  esac
}

main "$@"
